from dotenv import load_dotenv
import os
from pyzotero import zotero
import boto3


def get_paper_titles():
    load_dotenv()
    zot = zotero.Zotero(
        os.environ["ZOTERO_USER_ID"], "user", os.environ["ZOTERO_API_KEY"]
    )
    items = zot.top(limit=100)

    all_titles = [paper["data"]["title"] for paper in items]

    with open(
        "papers_reading_list.txt", "w"
    ) as f:  # needs to be updated to work with Mendeley API
        f.write("\n".join(all_titles))

    """
    boto_kwargs = {
        "aws_access_key_id": os.environ["AWS_ACCESS_KEY_ID"],
        "aws_secret_access_key": os.environ["AWS_SECRET_ACCESS_KEY"],
    }
    s3_client = boto3.Session(**boto_kwargs).client("s3")

    with open("papers_reading_list.txt", "rb") as f:
        s3_client.upload_fileobj(f, "wmaucla-papers", "papers_reading_list.txt")
        """


get_paper_titles()
