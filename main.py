import datetime
import os
import urllib.request
from datetime import datetime as dt

import numpy as np
from bs4 import BeautifulSoup
from dotenv import load_dotenv
from fastapi import FastAPI
from pyzotero import zotero
from sentence_transformers import SentenceTransformer, util

app = FastAPI()

load_dotenv()

def load_arxiv_data():
    """
    From ARXIV, get the most recent data
    """
    dates_list = []
    titles_list = []

    for url in [
        "http://export.arxiv.org/api/query?search_query=cat:cs.AI&start=0&max_results=50&sortBy=submittedDate&sortOrder=descending",
        "http://export.arxiv.org/api/query?search_query=cat:cs.CL&start=0&max_results=50&sortBy=submittedDate&sortOrder=descending",
    ]:
        page = urllib.request.urlopen(url)
        soup = BeautifulSoup(page, "html.parser")
        dates_list.extend(soup.find_all("updated"))
        titles_list.extend(soup.find_all("title"))

    return dates_list, titles_list


def load_zotero_data():
    zot = zotero.Zotero(
        os.environ["ZOTERO_USER_ID"], "user", os.environ["ZOTERO_API_KEY"]
    )
    items = zot.top(limit=100)

    return [paper["data"]["title"] for paper in items]


def main():
    # ============================================= Loading Data =============================================

    dates_list, titles_list = load_arxiv_data()
    sample_topics = load_zotero_data()

    model = SentenceTransformer("distilbert-base-nli-stsb-wkpooling")

    """
    # This eventually could be updated to be more topically / involves knowledge graphs - keeping code 
    topics = {"question answering": [], "privacy and justice": [], "knowledge graph": []}
    topics_list = list(topics.keys())
    sentence_embeddings = model.encode(topics_list, convert_to_tensor=True)
    # topics[topics_list[greatest_index]] = title.string
    """

    sentence_embeddings = model.encode(
        sample_topics, convert_to_tensor=True
    )  # embed the previous papers list

    prospective_papers = {}
    for title_date, title in zip(dates_list, titles_list):
        if (
            dt.today().date() - datetime.timedelta(days=1)
            == dt.strptime(title_date.string.split("T")[0], "%Y-%m-%d").date()
        ):  # check to make sure is published today
            # if ( dt(2020, 9, 24).date() == dt.strptime(title_date.string.split("T")[0], "%Y-%m-%d").date()):  # for testing purposes
            query_embedding = model.encode(title.string, convert_to_tensor=True)
            cos_scor = util.pytorch_cos_sim(query_embedding, sentence_embeddings)
            cos_scor = cos_scor.cpu()
            greatest_index = np.argmax(cos_scor.numpy())
            prospective_papers[title.string] = cos_scor[0][greatest_index]

    sorted_prospective_papers = {
        k: v for k, v in sorted(prospective_papers.items(), key=lambda item: item[1])
    }

    return list(sorted_prospective_papers.keys())[-5:]


@app.get("/")
async def root():
    return {"Hello!": "Navigate to /get_names"}


@app.get("/get_names")
async def return_names():
    return main()
